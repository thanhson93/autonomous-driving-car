# the whole system

**Main Function(automous-driving-car)**

## Stm32f1:
- LCD
- UART to communicate with beagle bone. 
- Read the sensor temperature and humility. 
- Send the data temperature and humility via UART.
- Control LED.
- Control the GPIO via PWM for the motor.
- Connect with RF. (2 mode is Wifi and RF)
- Upgradable firmware via UART based on the connection with beagle bone.
- Read ADC to monitor the voltage, temperature and current from board.
- Get the voltage on battery.

## Beaglebone
- Control Camera Logitech for detect the motion and line to go.
- Embedded the machine learning on. (If possible)
- UART configuration to communicate with stm32f1 and esp32.
- Receive data from Stm32f1 and send data to esp32 to sync up with website on host.
- Plug in the sd card for logging data. 
- Upgradable via SD card and OTA
- Use the Debian firmware first. (Can upgrade to build root or busybox to minimal the firmware)
- Encode the data transfer each of communication.
- Monitor the main program running (independence with application running.)
- Apply the algorithm for autonomous car via video get from camera.

## Website on host:
- Collect data and control device on. 
- Watch the video on host. (If possible)
- Control the motor via mqtt protocol.
- Send the command to board with configuration.
- Store the firmware to upgrade fw on.
- the database to store the firmware and old data. 
- Chart to display data from temperature and humility. Maybe a
- Decode the data receive from esp32

## Note
- Build a platform for HW and software.
- The software need easy to maintenance.
- The HW and software separate and handle for easy to moving the HW.
- It should have the configuration file.
- The best solution for HW is design my own board to execute.
￼
