#getting a key
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import serialization

private_key = rsa.generate_private_key(
    public_exponent = 65537, # what is this number
    key_size = 2048,
    backend = default_backend()
)
public_key = private_key.public_key()

private_pem = private_key.private_bytes(
    encoding = serialization.Encoding.PEM,
    format = serialization.PrivateFormat.PKCS8,
    encryption_algorithm = serialization.NoEncryption()
)

# store the private_key
with open('private_key.pem','wb') as f:
    f.write(private_pem)

public_pem = public_key.public_bytes(
    encoding = serialization.Encoding.PEM,
    format = serialization.PublicFormat.SubjectPublicKeyInfo
)

# store the public key
with open('public_key.pem', 'wb') as file:
    file.write(public_pem)
