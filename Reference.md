# BeagleBone Green Project
Note: I would like to refer one of these project BeagleBone below.

1. [Getting started with AWS IoT and BeagleBone](https://beagleboard.org/p/makers-ns/getting-started-with-aws-iot-and-beaglebone-396371)
 - Setup AWS IoT Thing and NodeJs client.
 - MQTT client

2. [AWS Panoptes](https://beagleboard.org/p/bginter/aws-panoptes-bbccc0)
 - Using AWS that monitors and reports observerd radio frequency spectral power and can be remotely controlled.
 - HW: NooElec NESDR Mini 2 USB RTL2832 + R820T2, Adafruit Ultimate GPS Breakout - 66 channel w/10 Hz updates - Version 3
 - SW: Mosquitto, Perl, Net::MQTT::Simple Perl Module, rtl_power_fftw, JSON Perl Module and AWS server

3. [Connected Cellular Beaglebone for IoT Development](https://beagleboard.org/p/agent-flynn-3/connected-cellular-beaglebone-for-iot-development-d61c8f)
 - Create an IoT device that doesnt have to be dependent on WIFI connection.
 - [Connected Cellular BeagleBone IoT Development Kit Digikey](https://www.digikey.com/en/product-highlight/a/aeris/connected-cellular-beaglebone-iot-development-kit)
 - This project had already finished by a resposibility on [Github](https://github.com/initialstate/cellular-beaglebone-iot-kit).

4. [Microsoft Azure Certified for IoT](https://beagleboard.org/p/bborgarchive/microsoft-azure-certified-for-iot-af45af)
 - This project helps bussinesses get started on their IoT projects quickly by connecting them with an ecosystem of partners that have offerings that can ..
 - BeagleBone collaborates with Microsoft to simplify IoT development.
 - Microsoft Azure Certified for IoT provides quick-start confidence to BeagleBone products with pre-tested software and hardware.

5. [Smart RV](https://beagleboard.org/p/DanielPatzer/smart-rv-96ddbd)
 - This project enables IoT applications for your camper van. The central control unit consist of a Python GUI on a BBB.
 - The central control unit consist of a 7 inch LCD capacitive touch screen and a beable bone black as a central computing unit.

6. [BBQBaileys' Home Security System](https://beagleboard.org/p/bbqandbanjos/bbqbaileys-home-security-system-95d5ab)
 - The system will interface to the Internet, a web server interface where it can display the current status, as well as acceptable commands,
 	use of the Tornado for the web server. It will be Object Oriented in it design. `DISPLAY MULTIPLE VIDEO CAMERA`....
 - [Desciption](http://bbqandbanjos.blogspot.com/2013/03/project-home-security-system.html)

7. [BlackLib C++ Library](https://beagleboard.org/p/ygtyce/blacklib-c-library-785c69)
 - BeagleBone Black C++ library for GPIO, ADC, PWM and etc.
 - BlackLib library is writen for controlling Beaglebone Black's feature.It take power from C++ language. Its created for reading analog input, generating pwm signal and using gpio pins. In addition to them, it includes debugging feature. So you can check errors after call any function in the library.

8. [Audio Video Streaming](https://beagleboard.org/p/rkohli-wordpress-com/a-v-streaming-be4eb1)
 - Audio and Video from camera and mic and string via Wlan.
 - No intruction and information about this project.
 - This project had made in Zinno Inc.

9. [Embedded Coder](https://beagleboard.org/p/bborgarchive/embedded-coder-eb9c54)
 - MathWorks support for BeagleBone Black.
 - You can design model by using Simulink, generate code using Embedded Coder and run executables on BeagleBone, a low-cost, single-board computer designed for digital signal progressing. The BeagleBone Black provides stereo audio and digital video connectivity, and support HDMI, Ethernet...

10. [Build your own Tiny Wifi camera](https://lifehacker.com/build-your-own-tiny-wi-fi-camera-5893516?popular=true)
 - There is many comment and the idea get into the tiny camera. 
 - No instruction or information about the product.
