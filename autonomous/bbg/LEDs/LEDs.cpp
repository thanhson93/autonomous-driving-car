#include <unistd.h>
#include <stdio.h>

using namespace std;

#define UNUSE(X) X=X

int main(int argc, char **argv)
{
    UNUSE(argc);
    UNUSE(argv);

    FILE *LEDHandle = NULL;

    const char *LEDBrightness = "/sys/class/leds/beaglebone:green:usr0/brightness";
    printf("\n Starting simple LED blink program \n");

    while (true)
    {
        LEDHandle = fopen(LEDBrightness, "r+");
        if(LEDHandle != NULL)
        {
            fwrite("1", sizeof(char), 1, LEDHandle);
            fclose(LEDHandle);
        }

        sleep(1);
        
        LEDHandle = fopen(LEDBrightness, "r+");
        if(LEDHandle != NULL)
        {
            fwrite("1", sizeof(char), 1, LEDHandle);
            fclose(LEDHandle);
        }

        sleep(1);
    }
    
}