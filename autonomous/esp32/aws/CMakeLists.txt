# The following four lines of boilerplate have to be in your project's CMakeLists
# in this exact order for cmake to work correctly
cmake_minimum_required(VERSION 3.5)

# (Not part of the boilerplate)
# This example uses an extra component for common functions such as Wi-Fi and Ethernet connection.
set(EXTRA_COMPONENT_DIRS component)

include($ENV{IDF_PATH}/tools/cmake/project.cmake)
project(prog_aws)

target_add_binary_data(${CMAKE_PROJECT_NAME}.elf "main/authority/client.crt" TEXT)
target_add_binary_data(${CMAKE_PROJECT_NAME}.elf "main/authority/client.key" TEXT)
