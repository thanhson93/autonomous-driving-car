#!/bin/sh
if [ -z ${IDF_PATH} ]; then
	echo "Export the PATH and IDF_PATH to compile"
	export IDF_PATH="/home/thanhson/gitlab/toolchain/esp-idf"
	export PATH="/home/thanhson/gitlab/toolchain/xtensa-esp32-elf/bin:$PATH"
else
	echo "IDF_PATH and PATH already added"
fi
