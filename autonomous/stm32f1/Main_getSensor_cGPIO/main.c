/*
 * STM32 driver for SPI ST7735
 *
 * Example usage with STM32F103C8T6
 *
 * ST7735 wiring example:
 * 1 VCC    - 3.3V Vcc
 * 2 GND    - GND
 * 3 CS     - PB5
 * 4 RESET  - PA12
 * 5 A0     - PA11
 * 6 SDA    - PB15 (SPI2 MOSI)
 * 7 SCK    - PB13 (SPI2 SCK)
 * 8 LED    - via resistor 68 Ohm to 3.3V Vcc
 *
 * Using hardware soft
 */

#include <stm32f10x.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>

#include "st7735.h"

/* declare the timing delay */
static __IO uint32_t TimingDelay = 0;

/* Declare the initialize structure for gpio */
GPIO_InitTypeDef  GPIO_InitStructure;

/* declare the function used */
void SysTick_Handler(void);
void Delay_US(__IO uint32_t nTime);

/* initialize the system tick for delay function */
void Initialize_systemtick(void)
{
  SysTick_Config(SystemCoreClock / 1000);
}

/* set the beginning value for LCD */
void SetBeginningValueST7735(void)
{
  ST7735_Init();
  ST7735_AddrSet(0,0,159,127);
  ST7735_Clear(COLOR565_WHITE);
  ST7735_Orientation(scr_CCW);
}

/* configure the unused gpio as analog input mode */
void Configuration_Unused_GPIO(void)
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, ENABLE);
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All; GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
  GPIO_Init(GPIOA, &GPIO_InitStructure); GPIO_Init(GPIOB, &GPIO_InitStructure); GPIO_Init(GPIOC, &GPIO_InitStructure); GPIO_Init(GPIOD, &GPIO_InitStructure); GPIO_Init(GPIOE, &GPIO_InitStructure);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE, DISABLE);
}

/* example excute display string on LCD */
void LCD_display_example(void)
{
  ST7735_Clear(COLOR565_WHITE);
  ST7735_PutStr7x11(0,  0, " !222222222", COLOR565_RED, COLOR565_LIME);
  ST7735_PutStr7x11(0, 12, "222222222222", COLOR565_RED, COLOR565_LIME);
  Delay_US(5000);
  ST7735_Clear(COLOR565_WHITE);
  ST7735_PutStr7x11(3, 4, " 11111111111111", COLOR565_RED, COLOR565_LIME);
  Delay_US(5000);
  ST7735_Clear(COLOR565_WHITE);
  ST7735_PutStr7x11(1, 2, "2x scale", COLOR565_RED, COLOR565_LIME);
  ST7735_PutStr7x11(2, 4, "5x7 font", COLOR565_RED, COLOR565_LIME);
  Delay_US(5000);
  ST7735_Clear(COLOR565_WHITE);
  ST7735_PutStr7x11(3, 5, "3x scale", COLOR565_RED, COLOR565_LIME);
  ST7735_PutStr7x11(3, 2, "5x7 font", COLOR565_RED, COLOR565_LIME);
  Delay_US(5000);
}
/* the main function will be call */
int main(void)
{
  Initialize_systemtick();

  Configuration_Unused_GPIO();

  SetBeginningValueST7735();

  /* excute display sample */
  while(1)
  {
    LCD_display_example();
  }
}

/* delay micro seconds */
void Delay_US(__IO uint32_t nTime)
{
  TimingDelay = nTime;
  while(TimingDelay != 0);
}

/* delay based on system tick handler */
void SysTick_Handler()
{
  if (TimingDelay != 0) TimingDelay--;
}
